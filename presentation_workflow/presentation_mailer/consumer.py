import json
import pika
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:
        def presentation_approval(ch, method, properties, body):
            print("Received %r" % body)
            context = json.loads(body)
            presenter_name = context["presenter_name"]
            title = context["title"]
            presenter_email = context["presenter_email"]
            recipient_list = [presenter_email]
            send_mail(
                'Your presentation has been accepted',
                f"{presenter_name}, we are happy to tell you that your presentation {title} has been accepted",
                'admin@conference.go',
                recipient_list,
                fail_silently=False,
            )
            print("sent approval letter")


        def presentation_rejection(ch, method, properties, body):
            print("Received %r" % body)
            context = json.loads(body)
            presenter_name = context["presenter_name"]
            title = context["title"]
            presenter_email = context["presenter_email"]
            recipient_list = [presenter_email]
            send_mail(
                'Your presentation has not been approved',
                f"{presenter_name}, we are sad to tell you that your presentation {title} has not been accepted",
                'admin@conference.go',
                recipient_list,
                fail_silently=False,
                )
            print("sent rejection letter")

        def main():
            parameters = pika.ConnectionParameters(host='rabbitmq')
            connection = pika.BlockingConnection(parameters)
            channel = connection.channel()

            channel.queue_declare(queue='approvedtask')
            channel.basic_consume(
                queue='approvedtask',
                on_message_callback=presentation_approval,
                auto_ack=True,
            )

            channel.queue_declare(queue="rejectedtask")
            channel.basic_consume(
                queue='rejectedtask',
                on_message_callback=presentation_rejection,
                auto_ack=True,
            )
            channel.start_consuming()

        main()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)

# def process_approval(ch, method, properties, body):
#     message = json.loads(body)
#     presenter_name = message["presenter_name"]
#     presenter_email = message["presenter_email"]
#     title = message["title"]
#     send_mail('Your presentation has been accepted',
#               f"{presenter_name}, we're happy to tell you that your presentation {title} has been accepted",
#               f"{presenter_email}",
#               ["admin@conference.go"],
#               fail_silently=False,)


#     parameters = pika.ConnectionParameters(host='rabbitmq')
#     connection = pika.BlockingConnection(parameters)
#     channel = connection.channel()
#     channel.queue_declare(queue='presentations_approvals')
#     channel.basic_consume(
#         queue='presentation_approvals',
#         on_message_callback=process_approval,
#         auto_ack=True,
#     )
#     channel.start_consuming()

# def process_rejection(ch, method, properties, body):
#     message = json.loads(body)
#     presenter_name = message["presenter_name"]
#     presenter_email = message["presenter_email"]
#     title = message["title"]
#     send_mail('Your presentation has been rejected',
#               f"{name}, Unfortunately, your presentation {title} has been rejected",
#               "test@gmail.com",
#               ["admin@conference.go"],
#               fail_silently=False,)



#     parameters = pika.ConnectionParameters(host='rabbitmq')
#     connection = pika.BlockingConnection(parameters)
#     channel = connection.channel()
#     channel.queue_declare(queue='presentations_approvals')
#     channel.basic_consume(
#         queue='presentation_approvals',
#         on_message_callback=process_approval,
#         auto_ack=True,
#     )
#     channel.start_consuming()
