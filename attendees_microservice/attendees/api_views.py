from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

# from events.api_views import ConferenceListEncoder
from common.json import ModelEncoder
from .models import Attendee, ConferenceVO, AccountVO
# from events.models import Conference


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created"
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }

    def get_extra_data(self, o):
        count = AccountVO.objects.filter(email=o.account.email)
        return {"has_account": count > 0}
        # count = 0
        # dict = {}
        # dict["has_account"] = False

        # for account in AccountVO.objects.all():
        #     if account["email"] == o.email:
        #         count += 1

        # if count:
        #     dict["has_account"] = True

        # return dict



@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            conference_href = f'/api/conferences/{conference_vo_id}/'
            print(ConferenceVO.objects.all())
            conference = ConferenceVO.objects.get(import_href=conference_href)
            print(conference, "**************")
            content["conferences"] = conference
        except ConferenceVO.DoesNotExist:
            print(conference_vo_id)
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


def api_show_attendee(request, pk):
    """
    Returns the details for the Attendee model specified
    by the pk parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    attendee = Attendee.objects.get(id=pk)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )
